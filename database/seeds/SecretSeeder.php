<?php

use Illuminate\Database\Seeder;
use App\Model\Secret;

class SecretSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rootAccessKey = getenv('ROOT_ACCESS_KEY');
        $rootAccessSecret = getenv('ROOT_ACCESS_SECRET');

        Secret::create([
            'access_key' => $rootAccessKey,
            'access_secret' => $rootAccessSecret
        ]);
    }
}
