<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Secret extends Model
{
    protected $fillable = ['access_key', 'access_secret'];
}