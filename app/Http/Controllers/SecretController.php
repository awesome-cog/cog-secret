<?php
namespace App\Http\Controllers;


use App\Model\Secret;
use Illuminate\Http\Request;

class SecretController extends Controller
{
    public function storage(Request $request)
    {
        $this->validate($request, [
            'access_key' => 'required',
            'access_secret' => 'required'
        ]);

        $secret = new Secret($request->all());
        $secret->save();
        return $secret;
    }

    public function get(Request $request)
    {
        $this->validate($request, [
            'access_key' => 'required',
            'access_secret' => 'required'
        ]);

        $secret = Secret::where('access_key', $request->get('access_key'))
            ->where('access_secret', $request->get('access_secret'))
            ->first();

        if (null == $secret)
            abort(404, "Not found");

        return $secret;
    }
}